// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users')

// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] User Routes
	// Register User
	route.post('/register', (req, res) => {
		let data = req.body;
		controller.registerUser(data).then(result => 
			res.send(result));
	});

	// Retrieve Single User
	route.get('/:id', (req, res) => {
		let userId = req.params.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		})
	});

	// Retrieve All Users
	route.get('/',(req, res) => {
		controller.getAllUsers().then(result => {
			res.send(result);
		});
	});

	// Delete user profile
	route.delete('/:id', (req,res) => {
		let userId = req.params.id;
		controller.deleteUser(userId).then(outcome => {
			res.send(outcome);
		})
	});

	// Update User Profile
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let katawan = req.body
		controller.updateUser(id, katawan).then(outcome => {
			res.send(outcome);
		})
	});

// [SECTION] Expose Route System
	module.exports = route;
