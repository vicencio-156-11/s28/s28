// Create a schema, model for the task collection and make sure to export
const mongoose = require('mongoose');

const taskBlueprint = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Task Name is Required']
	},
	status: {
		type: String,
		default: 'pending'
	}
});

// create a model using a Schema
module.exports = mongoose.model("Task",taskBlueprint);
