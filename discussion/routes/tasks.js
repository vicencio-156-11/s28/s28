// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Tasks Routes
	// Create Task
	route.post('/', (req,res) => {
		let taskInfo = req.body;
		controller.createTask(taskInfo).then(result => 
			res.send(result))
	});

	// Retrieve Single User
	route.get('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.getTask(taskId).then(outcome => {
			res.send(outcome);
		})
	});

	// Update User Profile
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let katawan = req.body
		controller.updateTask(id, katawan).then(outcome => {
			res.send(outcome);
		})
	});

	// Delete user profile
	route.delete('/:id', (req,res) => {
		let taskId = req.params.id;
		controller.deleteTask(taskId).then(outcome => {
			res.send(outcome);
		})
	});


// [SECTION] Expose Route System
	module.exports = route;

	// Retrieve all Tasks
	route.get('/',(req,res) => {
		controller.getAllTasks().then(result => {
			res.send(result);
		})
	});

// [SECTION] Expose Route System
	module.exports = route;